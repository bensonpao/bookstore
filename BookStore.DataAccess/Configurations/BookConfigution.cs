﻿

using BookStorage.Core.Models;
using BookStore.DataAccess.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookStore.DataAccess.Configurations
{
    public class BookConfigution : IEntityTypeConfiguration<BookEntity>
    {
        public void Configure(EntityTypeBuilder<BookEntity> builder)
        {
            builder.HasKey(o=> o.Id);
            builder.Property(o=> o.Title).HasMaxLength(Book.MAX_TITLE_LENGTH).IsRequired();
            builder.Property(o => o.Description).IsRequired();
            builder.Property(o => o.Price).IsRequired();

        }
    }
}
