﻿

using BookStorage.Core.Models;
using BookStore.DataAccess.Entites;
using Microsoft.EntityFrameworkCore;
using BookStore.Core.Abstractions;

namespace BookStore.DataAccess.Repostories
{
    public class BooksRepostory : IBooksRepostory
    {   
        private readonly BookStoreDBContext _context;
        public BooksRepostory(BookStoreDBContext context) {
            _context = context;
        }

        public async Task<(List<Book>?, string?)> Get()
        {
            try
            {
                var bookEntities = await _context.Books.AsNoTracking().ToListAsync();
                var books = bookEntities.Select(o => Book.Create(o.Id, o.Title, o.Description, o.Price).Book).ToList();
                return (books, null);
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }

        public async Task<(Guid?, string? Error)> Create(Book book)
        {
            var bookEntity = new BookEntity
            {
                Id = book.Id,
                Title = book.Title,
                Description = book.Description,
                Price = book.Price
            };

            try
            {
                await _context.AddAsync(bookEntity);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
            return (bookEntity.Id, null);
        }

        public async Task<(int?, string?)> Update(Guid id, string title, string description, decimal price) {
            try
            {
                var bookEntity = await _context.Books.FirstOrDefaultAsync(o => o.Id == id);
                if (bookEntity != null)
                {
                    bookEntity.Title = title;
                    bookEntity.Description = description;
                    bookEntity.Price = price;

                    var count = await _context.SaveChangesAsync();
                    return (count, null); 
                }
                else
                {
                    return (null, "Book not found");
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }

        }

        public async Task<(int?, string?)> Delete(Guid id)
        {
            try
            {
                var bookEntity = await _context.Books.FirstOrDefaultAsync(o => o.Id == id);
                if (bookEntity != null)
                {
                    _context.Remove(bookEntity);
                    var count = await _context.SaveChangesAsync();
                    return (count, null);
                }
                else
                {
                    return (null, "Book not found");
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }

        }
    }
}
