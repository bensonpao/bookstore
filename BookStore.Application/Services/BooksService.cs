﻿using BookStorage.Core.Models;
using BookStore.Core.Abstractions;
using BookStore.DataAccess.Repostories;
using System.Runtime.InteropServices;

namespace BookStore.Application.Services
{
    public class BooksService : IBooksService
    {
        private readonly IBooksRepostory _booksRepository;

        public BooksService(IBooksRepostory bookRepository) {
            _booksRepository = bookRepository;
        }

        public async Task<List<Book>?> GetAllBooks() {
            (List<Book>? List, string? err) = await _booksRepository.Get();
            return List;
        }

        public async Task<Guid?> Create(Book book) {
            (Guid? id, string? err) = await _booksRepository.Create(book);
            return id;
        }

        public async Task<int?> Update(Guid id, string title, string description, decimal price) {
            (int? count, string? err) = await _booksRepository.Update(id, title, description, price);
            return count;
        }

        public async Task<int?> Delete(Guid id) {
            (int? count, string? err) = await _booksRepository.Delete(id);
            return count;
        }
  
    }
}




//Task<(int?, string?)> Update(Guid id, string title, string description, decimal price);
//Task<(int?, string?)> Delete(Guid id);
