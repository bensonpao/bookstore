﻿using BookStorage.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.Abstractions
{
    public interface IBooksService
    {
        Task<List<Book>?> GetAllBooks();
        Task<Guid?> Create(Book book);
        Task<int?> Update(Guid id, string title, string description, decimal price);
        Task<int?> Delete(Guid id);
    }
}
