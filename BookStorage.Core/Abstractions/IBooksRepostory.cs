﻿using BookStorage.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Core.Abstractions
{
    public interface IBooksRepostory
    {
        Task<(List<Book>?, string?)> Get();
        Task<(Guid?, string? Error)> Create(Book book);
        Task<(int?, string?)> Update(Guid id, string title, string description, decimal price);
        Task<(int?, string?)> Delete(Guid id);
    }
}
