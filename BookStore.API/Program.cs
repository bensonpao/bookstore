
using BookStore.DataAccess;
using Microsoft.EntityFrameworkCore;
using BookStore.Core.Abstractions;
using BookStore.Application.Services;
using BookStore.DataAccess.Repostories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddDbContext<BookStoreDBContext>(options =>
{
    if (builder.Environment.IsDevelopment())
    {
        options.UseSqlServer(builder.Configuration.GetConnectionString("Development"), b => b.MigrationsAssembly("BookStore.API"));
    }
    else {
        options.UseSqlServer(builder.Configuration.GetConnectionString("Production"), b => b.MigrationsAssembly("BookStore.API"));
    }
});

builder.Services.AddScoped<IBooksService, BooksService>();
builder.Services.AddScoped<IBooksRepostory, BooksRepostory>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseCors(o =>
{
    o.WithHeaders().AllowAnyHeader();
    o.WithMethods().AllowAnyMethod();
    if (builder.Environment.IsDevelopment())
    {
        o.WithOrigins().AllowAnyOrigin();
    }
    else
    {
        o.WithOrigins().WithOrigins("", "");
    }

});

app.Run();
