﻿
using Microsoft.AspNetCore.Mvc;

using BookStore.Core.Abstractions;
using BookStore.DataAccess.Repostories;
using BookStore.API.Contracts;
using BookStorage.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace BookStore.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBooksService _BooksService;
        public BooksController(IBooksService services) {
            _BooksService = services;
        }

        [HttpGet]
        public async Task<ActionResult<List<BooksRepostory>>> GetBooks()
        {
            var books = await _BooksService.GetAllBooks();
            if (books == null)
            {
                return StatusCode(400, "Error");
            }
            var rep = books.Select(o => new BooksResponse(o.Id, o.Title, o.Description, o.Price));
            return Ok(rep);
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Create([FromBody] BooksRequest request)
        {
            var (book, error) = Book.Create(Guid.NewGuid(), request.Title, request.Description, request.Price);
            if (string.IsNullOrEmpty(error))
            {
                return StatusCode(400, error);
            }
            var result = await _BooksService.Create(book);
            if (result == null)
            {
                return StatusCode(400, "Error");
            }
            return Ok(result);
        }

        [Route("{id:guid}")]
        [HttpPut]
        public async Task<ActionResult<int>> Update(Guid id, [FromBody] BooksRequest request)
        {
            var count = await _BooksService.Update(id, request.Title, request.Description, request.Price);
            if (count == null) {
                return StatusCode(400, "Error");
            }
            return(Ok(count));
        }

        [Route("{id:guid}")]
        [HttpDelete]
        public async Task<ActionResult<int>> Delete(Guid id)
        {
            var count =  await _BooksService.Delete(id);
            if (count == null)
            {
                return StatusCode(400, "Error");
            }
            return (Ok(count));
        }
    }
}
